#include "JetAnalysis/JetAnalysisAlg.h"
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include "StoreGate/ReadHandle.h"


JetAnalysisAlg :: JetAnalysisAlg (const std::string& name,ISvcLocator *pSvcLocator):
    AthAlgorithm (name, pSvcLocator)
    , m_ntsvc("THistSvc/THistSvc", name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
  //declareProperty( "ElectronPtCut", m_electronPtCut = 25000.0,
 //                  "Minimum electron pT (in MeV)" );
 // declareProperty( "SampleName", m_sampleName = "Unknown",
 //                  "Descriptive name for the processed sample" );

}



StatusCode JetAnalysisAlg :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  ATH_MSG_INFO ("in initialize");

    
  // Book the variables to save in the *.root 
  m_Tree = new TTree("dumpedData", "dumpedData");
  ATH_MSG_DEBUG("Booking branches...");
  bookBranches(m_Tree); // book TTree with branches
  if (!m_ntsvc->regTree("/rec/", m_Tree).isSuccess()) {
      ATH_MSG_ERROR("could not register tree [dumpedData]");
      return StatusCode::FAILURE;
    }
  return StatusCode::SUCCESS;
}


StatusCode JetAnalysisAlg :: execute ()
{
  //retrieve the eventInfo object from the event store 
  const xAOD::EventInfo *eventInfo = nullptr;
  ATH_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
  
  // print out run and event number from retrieved object
  ATH_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

  // truth matching 
  if(!dumpTruthLLP()) ATH_MSG_DEBUG("ALP Truth Event information cannot be collected!");

  // loop over jets in the container
  const xAOD::JetContainer* jets = nullptr;
  ATH_CHECK (evtStore()->retrieve(jets, "AntiKt4EMTopo"));
  ATH_MSG_INFO("Number of jets: " <<  jets->size());
  
  for (const xAOD::Jet* jet : *jets){ //loop over jets in container
    ATH_MSG_INFO ("Jet:   pt: " << jet->pt() << " MeV" << ". Eta; Phi: " << jet->eta() << "; " << jet->phi());
    double jetPt   = jet->pt();
    double jetEta  = jet->eta();
    double jetPhi  = jet->phi();
    float dR2;

    for (const xAOD::TruthParticle* part : *mc_alp){
    dR2  = std::pow( std::acos( std::cos( jetPhi - part->phi() ) ) , 2 );
    dR2 += std::pow( jetEta - part->eta() , 2 );
    
    if (dR2<= 0.2 * 0.2){
      ATH_MSG_INFO(part->pdgId()<<" match with jet (eta,phi) :" << jet->eta() << ", " << jet->phi());
      mc_alp_jet_pt->push_back(jet->pt());
      mc_alp_jet_eta->push_back(jet->eta());
      mc_alp_jet_phi->push_back(jet->phi());
      break;
      }
    }
    j_jetEta->push_back(jetPt);
    j_jetPhi->push_back(jetEta);
    j_jetPt->push_back(jetPhi);
  } 
  // end of loop

  m_Tree->Fill();
  clear();
  return StatusCode::SUCCESS;
}


StatusCode JetAnalysisAlg :: finalize ()
{

  return StatusCode::SUCCESS;
}

StatusCode JetAnalysisAlg::dumpTruthLLP(){
  const xAOD::TruthParticleContainer *truthParticleCnt = nullptr;
  ATH_CHECK (evtStore()->retrieve (truthParticleCnt, "TruthParticles")); // retrieve container
  for (const xAOD::TruthParticle* part : *truthParticleCnt){
    if(part->pdgId()==35){
    mc_alp->push_back(const_cast<xAOD::TruthParticle*>(part));//container 
    mc_alp_energy->push_back(part->e());
    mc_alp_pt->push_back(part->pt());
    mc_alp_m->push_back(part->m());
    mc_alp_eta->push_back(part->eta());
    mc_alp_phi->push_back(part->phi());
    mc_alp_decay_x->push_back(part->decayVtx()->x());
    mc_alp_decay_y->push_back(part->decayVtx()->y());
    mc_alp_decay_z->push_back(part->decayVtx()->z());
    mc_alp_decay_Lxy->push_back(sqrt(pow(part->decayVtx()->x(),2) + pow(part->decayVtx()->y(),2)));
    ATH_MSG_INFO("Lxy of this event " << sqrt(pow(part->decayVtx()->x(),2) + pow(part->decayVtx()->y(),2)));
    }
  }
  return StatusCode::SUCCESS;
}

void JetAnalysisAlg::clear(){
  // ## Jet
  j_jetPt->clear();
  j_jetEta->clear();
  j_jetPhi->clear();

  // alp 
  mc_alp_energy->clear();
  mc_alp_pt->clear();
  mc_alp_m->clear();
  mc_alp_eta->clear();
  mc_alp_phi->clear();
  mc_alp_eta->clear();
  mc_alp_decay_x->clear();
  mc_alp_decay_y->clear();
  mc_alp_decay_z->clear();
  mc_alp_decay_Lxy->clear(); 

  // ALP jet match
  mc_alp_jet_pt->clear();
  mc_alp_jet_eta->clear();
  mc_alp_jet_phi->clear();

  // container 
  mc_alp->clear();
  match_j->clear();

}

void JetAnalysisAlg::bookBranches(TTree *tree){
  tree->Branch ("jet_pt",&j_jetPt);
  tree->Branch ("jet_eta",&j_jetEta);
  tree->Branch ("jet_phi",&j_jetPhi);

  tree->Branch("alp_energy", &mc_alp_energy);
  tree->Branch("alp_pt", &mc_alp_pt);
  tree->Branch("alp_m", &mc_alp_m);
  tree->Branch("alp_eta", &mc_alp_eta);
  tree->Branch("alp_phi", &mc_alp_phi);
  tree->Branch("alp_eta", &mc_alp_eta);
  tree->Branch("alp_decay_x", &mc_alp_decay_x);
  tree->Branch("alp_decay_y", &mc_alp_decay_y);
  tree->Branch("alp_decay_z", &mc_alp_decay_z); 
  tree->Branch("alp_decay_Lxy", &mc_alp_decay_Lxy);
  
  /// ALP match jets
  tree->Branch("alp_jet_pt", &mc_alp_jet_pt);
  tree->Branch("alp_jet_eta", &mc_alp_jet_eta);
  tree->Branch("alp_jet_phi", &mc_alp_jet_phi);
}
