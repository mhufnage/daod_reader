#!/usr/bin/env python
###############################################################
#
# Job options file
#
#==============================================================
if __name__=='__main__':

    import argparse
    import sys,os

    parser = argparse.ArgumentParser(description = '', add_help = False)
    parser = argparse.ArgumentParser()

    #--------------------------------------------------------------
    # File configuration
    #--------------------------------------------------------------

    parser.add_argument('-i','--inputFiles', action='store', dest='inputFiles', required = False,
                      help = "Comma-separated list of input files (alias for --filesInput)")

    parser.add_argument('-o','--outputFile', action='store', dest='outputFile', required = False,
                        default = 'reader_dumpedOutput.root',
                        help = "Output root file.")

    parser.add_argument('--nov','--numberOfEvents', action='store', dest='numberOfEvents', required = False, 
                      type=int, default=-1,
                      help = "The number of events to run.")

    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()

    #--------------------------------------------------------------
    # ATLAS default Application Configuration options
    #--------------------------------------------------------------
    import AthenaCommon.AtlasUnixStandardJob

    #--------------------------------------------------------------
    # Private Application Configuration options
    #--------------------------------------------------------------
    isMC = False

    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon.AthenaCommonFlags import jobproperties as jps

    #### Geometry relation flags
    from AthenaCommon.GlobalFlags import jobproperties
    from AthenaCommon.DetFlags import DetFlags
    from AthenaCommon.GlobalFlags import globalflags

    Geometry = "ATLAS-R2-2016-01-00-01"
    globalflags.DetGeo.set_Value_and_Lock('atlas')
    #### ! If data is MC, comment the line below! #####
    if not(isMC):
        globalflags.DataSource.set_Value_and_Lock('data') #
    ###################################################
    DetFlags.detdescr.all_setOn()
    DetFlags.Forward_setOff()
    DetFlags.ID_setOff()

    jobproperties.Global.DetDescrVersion = Geometry

    # We need the following two here to properly have Geometry
    from AtlasGeoModel import GeoModelInit
    from AtlasGeoModel import SetGeometryVersion
    include("CaloDetMgrDetDescrCnv/CaloDetMgrDetDescrCnv_joboptions.py")
    include("LArDetDescr/LArDetDescr_joboptions.py")
    #####

    # Cabling map acess (LAr)
    from LArCabling.LArCablingAccess import LArOnOffIdMapping
    LArOnOffIdMapping()

    from AthenaCommon.Logging import log
    from AthenaCommon.Constants import INFO
    log.setLevel(INFO)


# ----------------- NEW  -----------------------------
    # Set the Athena configuration flags
    from AthenaConfiguration.AllConfigFlags import ConfigFlags

    if args.inputFiles is not None:
        ConfigFlags.Input.Files = args.inputFiles.split(',')
    ConfigFlags.Input.isMC = isMC
    ConfigFlags.Output.HISTFileName = args.outputFile
    
    ConfigFlags.lock()

    # Initialize configuration object, add accumulator, merge, and run.
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg 
    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    cfg = MainServicesCfg(ConfigFlags)
    cfg.merge(PoolReadCfg(ConfigFlags))

    from JetAnalysis.JetAnalysisConfig import JetAnalysisCfg
    JetAnalysisAcc = JetAnalysisCfg()
    cfg.merge(JetAnalysisAcc)

    cfg.printConfig(withDetails=False) # set True for exhaustive info
    sc = cfg.run(args.numberOfEvents) #use cfg

    sys.exit(0 if sc.isSuccess() else 1)
# ----------------- NEW  -----------------------------

# ----------------- OLD STYLE -----------------------------
    # Full job is a list of algorithms
    # from AthenaCommon.AlgSequence import AlgSequence
    # from JetAnalysis.JetAnalysisConf import JetAnalysisAlg

    # job = AlgSequence()

    # job += JetAnalysisAlg( "JetAnalysis" )

    # job.JetAnalysis.clusterName     = "LArClusterEM7_11Nocorr"#"CaloCalTopoClusters"#"LArClusterEM7_11Nocorr"#"egammaClusters"#
    # job.JetAnalysis.jetName         = "AntiKt4EMPFlowJets"
    # job.JetAnalysis.roiRadius       = 0.2
    # job.JetAnalysis.tileDigName     = "TileDigitsCnt"
    # if isMC:
    #     job.JetAnalysis.larDigName      = "LArDigitContainer_MC"
    # else:
    #     job.JetAnalysis.larDigName      = 'FREE' #'LArDigitContainer_EMClust'#'LArDigitContainer_Thinned' #
    # job.JetAnalysis.tileRawName     = "TileRawChannelCnt"
    # job.JetAnalysis.larRawName      = "LArRawChannels"
    # job.JetAnalysis.doTile          = True
    # job.JetAnalysis.noBadCells      = True
    # job.JetAnalysis.doLAr           = True
    # job.JetAnalysis.printCellsClus  = False
    # job.JetAnalysis.printCellsJet   = False
    # job.JetAnalysis.testCode        = False
    # job.JetAnalysis.OutputLevel     = DEBUG # INFO #
    # job.JetAnalysis.isMC            = isMC  # set to True in case of MC sample.

    # from AthenaCommon.AppMgr import ServiceMgr
    # import AthenaPoolCnvSvc.ReadAthenaPool

    # # inputDataPath   = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/lowPileUpRun_RAWtoESD/'
    # if args.inputFiles is not None:
    #     ServiceMgr.EventSelector.InputCollections = args.inputFiles.split(',')
    # ServiceMgr += CfgMgr.THistSvc()

    # # Create output file
    # hsvc = ServiceMgr.THistSvc
    # # jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:myfile.root"]
    # # hsvc.Output += [ "rec DATAFILE='run329542_minBias/ESD_1_dumped.root' OPT='RECREATE'" ]
    # hsvc.Output += [ "rec DATAFILE='ntuples/test_minbias_globalCablingTest.root' OPT='RECREATE'" ]
    # theApp.EvtMax = args.numberOfEvents

    # MessageSvc.defaultLimit = 9999999  # all messages
#  ----------------- OLD STYLE -----------------------------



# athena.py JetAnalysis_jobOptions.py  > reader_test_DATA.log 2>&1; code reader_test_DATA.log



# ###############################################################
# #
# # Job options file
# #
# #==============================================================
# #--------------------------------------------------------------
# # ATLAS default Application Configuration options
# #--------------------------------------------------------------
# import AthenaCommon.AtlasUnixStandardJob

# #--------------------------------------------------------------
# # Private Application Configuration options
# #--------------------------------------------------------------
# # doFwd = False
# # doMC = True
# # dumpCells = False
# # doPhoton = False
# from AthenaCommon.AppMgr import ToolSvc
# from AthenaCommon.AthenaCommonFlags import jobproperties as jps

# # Full job is a list of algorithms
# from AthenaCommon.AlgSequence import AlgSequence
# job = AlgSequence()

# from JetAnalysis.JetAnalysisConf import JetAnalysisAlg

# #### Geometry relation flags
# from AthenaCommon.GlobalFlags import jobproperties
# from AthenaCommon.DetFlags import DetFlags
# from AthenaCommon.GlobalFlags import globalflags

# Geometry = "ATLAS-R2-2016-01-00-01"
# globalflags.DetGeo.set_Value_and_Lock('atlas')
# DetFlags.detdescr.all_setOn()
# DetFlags.Forward_setOff()
# DetFlags.ID_setOff()

# jobproperties.Global.DetDescrVersion = Geometry

#     # We need the following two here to properly have
#     # Geometry
# from AtlasGeoModel import GeoModelInit
# from AtlasGeoModel import SetGeometryVersion
# include("CaloDetMgrDetDescrCnv/CaloDetMgrDetDescrCnv_joboptions.py")
# include("LArDetDescr/LArDetDescr_joboptions.py")
# #####

# # Cabling map acess (LAr)
# from LArCabling.LArCablingAccess import LArOnOffIdMapping
# LArOnOffIdMapping()

# job += JetAnalysisAlg( "JetAnalysis" )

# job.JetAnalysis.clusterName     = "CaloCalTopoClusters"
# job.JetAnalysis.jetName         = "AntiKt4EMPFlowJets"
# job.JetAnalysis.roiRadius       = 0.2
# job.JetAnalysis.tileDigName     = "TileDigitsCnt"
# job.JetAnalysis.larDigName      = "LArDigitContainer_MC"
# job.JetAnalysis.tileRawName     = "TileRawChannelCnt"
# job.JetAnalysis.larRawName      = "LArRawChannels"
# job.JetAnalysis.doTile          = True
# job.JetAnalysis.noBadCells      = True
# job.JetAnalysis.doLAr           = True
# job.JetAnalysis.printCellsClus  = False
# job.JetAnalysis.printCellsJet   = False
# job.JetAnalysis.testCode        = False
# # job.JetAnalysis.OuputLevel = INFO

# from AthenaCommon.AppMgr import ServiceMgr
# import AthenaPoolCnvSvc.ReadAthenaPool


# from os import system

# if not 'FileName' in dir():
#     FileName = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_evenMoreEvents/ESD_pi0_000001.pool.root'
# if not 'OutputFileName' in dir():
#     OutputFileName='ntuple.root'

# # testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD/ESD_pi0.pool.root"

# # testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_RDO_LarDigits/ESD_fromRDO-HITS_PI0_001783.pool.root"
# # testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_AMItags/ESD_pi0.pool.root" #AMI TAG reco
# # testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_AMIdumpCells/ESD_pi0.pool.root"
# # inputFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_moreEvents/ESD_pi0.pool.root"

# # inputFile = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_evenMoreEvents/ESD_pi0_002450.pool.root'
# # inputFile = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_evenMoreEvents/ESD_pi0_000001.pool.root'
# ServiceMgr.EventSelector.InputCollections = [ FileName ]
# ServiceMgr += CfgMgr.THistSvc()

# # Create output file
# hsvc = ServiceMgr.THistSvc
# # jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:myfile.root"]
# hsvc.Output += [ "rec DATAFILE=\'{}\' OPT='RECREATE'" .format(OutputFileName) ]
# # hsvc.Output += [ "recHist DATAFILE='hist.root' OPT='RECREATE'" ]
# theApp.EvtMax = -1

# MessageSvc.defaultLimit = 9999999  # all messages