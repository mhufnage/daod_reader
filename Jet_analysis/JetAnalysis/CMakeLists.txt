## This file goes INSIDE a package directory.
## Its responsible to point to all files within the project, in order to compile them.
## automatically generated CMakeLists.txt file

# Declare the package
ATLAS_SUBDIR( JetAnalysis )

find_package( COMPONENTS )
# Add binary
# ATLAS_ADD_LIBRARY ( JetAnalysisLib JetAnalysis/JetAnalysisAlg.h Root/JetAnalysisAlg.cxx #Root/components/*.cxx
# 		  PUBLIC_HEADERS JetAnalysis
#           LINK_LIBRARIES xAODEventInfo
#                  )

atlas_add_component ( JetAnalysis JetAnalysis/*.h Root/*.cxx Root/components/*.cxx
       PUBLIC_HEADERS JetAnalysis
       # INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
       LINK_LIBRARIES AthenaBaseComps xAODEventInfo xAODCaloEvent xAODTruth xAODEgamma CaloEvent LArIdentifier LArRecEvent LArCablingLib CaloUtilsLib AthContainers Identifier LArRawEvent xAODJet TileEvent TileIdentifier TileConditionsLib xAODTracking EgammaAnalysisInterfacesLib #xAODTrigEgamma
)

# Install python modules, joboptions, and share content
atlas_install_python_modules( python/*.py )
atlas_install_joboptions( share/*.py )
